typedef struct data {
	char name[16];
	unsigned int age;
}data;

typedef struct node {
	data d1;
	struct node *next;
}node;

typedef struct queue {
	node *head, *tail;
}queue;

void qinit(queue *q);
void enq(queue *q, data d2);
data deq(queue *q);
int qfull(queue *q);
int qempty(queue *q);
